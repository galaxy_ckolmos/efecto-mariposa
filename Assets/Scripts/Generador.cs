using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class Generador : MonoBehaviour
{
    public GameObject prefab;
    public GameObject prefabCubo;
    public GameObject personajeRaro;
    public Material[] materiales = new Material[5];
    public TextMeshProUGUI texto1;
    public TextMeshProUGUI texto2;
    public Animator personaje;
    public GameObject bocadillo;
    public GameObject panel;
    public AudioSource audioPlink;
    public AudioSource slipUp;
    public AudioSource slipDown;
    public AudioSource pop;
    public AudioSource tada;

    int aciertos = 0;
    int fallos = 0;
    int restantes = 0;

    int linea;

    public bool jugando = false;
    bool gameOver = false;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(2);
        personaje.enabled = true;
        slipUp.Play();
        yield return new WaitForSeconds(1);
        
        //bocadillo.transform.DOScaleY(0, 0);
        bocadillo.SetActive(true);
        audioPlink.Play();
        bocadillo.transform.DOPunchScale(Vector3.one*10, 0.25F);
    }

    IEnumerator Iniciar()
    {
        yield return new WaitForSeconds(2);
        //personaje.gameObject.SetActive(false);
        bocadillo.SetActive(false);
        panel.SetActive(true);
        jugando = true;
        personaje.GetComponent<Animator>().SetTrigger("salir");
        slipDown.Play();

        for (int i = 0; i < 100; i++)
        {
            GameObject instancia;

            if (Random.Range(0, 100) < 10)
            {
                

                instancia = Instantiate(prefabCubo, new Vector3(Random.Range(-9, 9), 10, Random.Range(-3f, 3f)), Quaternion.identity);
                instancia.GetComponent<Esfera>().esfera = false;
            }
            else
            {
                instancia = Instantiate(prefab, new Vector3(Random.Range(-9, 9), 10, Random.Range(-3f, 3f)), Quaternion.identity);
                instancia.GetComponent<Esfera>().esfera = true;
            }

            instancia.GetComponent<Rigidbody>().mass = Random.Range(0.25F, 2F);
            instancia.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(0,109F), Random.Range(0, 190F), Random.Range(0, 100F)));
            int mat = Random.Range(0, 10);
            if (mat == 0) mat = 2;
            else mat = Random.Range(0, 2);
            instancia.GetComponent<Esfera>().material = mat;
            instancia.GetComponent<Renderer>().material = materiales[mat];

            if (instancia.GetComponent<Esfera>().esfera && instancia.GetComponent<Esfera>().material == 2) restantes++;

            yield return new WaitForSeconds(0.1f);
        }

        while(true && jugando)
        {
            GameObject instancia;

            if (Random.Range(0, 100) < 10)
            {
                instancia = Instantiate(prefabCubo, new Vector3(Random.Range(-9, 9), 10, Random.Range(-3f, 3f)), Quaternion.identity);
            }
            else
            {
                instancia = Instantiate(prefab, new Vector3(Random.Range(-9, 9), 10, Random.Range(-3f, 3f)), Quaternion.identity);
            }
            instancia.GetComponent<Rigidbody>().mass = Random.Range(0.25F, 2F);
            instancia.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(0, 109F), Random.Range(0, 190F), Random.Range(0, 100F)));
            int mat = Random.Range(0, 10);
            if (mat == 0) mat = 2;
            else mat = Random.Range(0, 2);
            instancia.GetComponent<Renderer>().material = materiales[mat];

            if (instancia.GetComponent<Esfera>().esfera && instancia.GetComponent<Esfera>().material == 2) restantes++;

            yield return new WaitForSeconds(1);
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && jugando)
        {
            if (Random.Range(0, 100) > 80)
            {
                if(jugando)
                    EfectoAleatorio();
            }
        }
    }

    void EfectoAleatorio()
    {
        int efecto = Random.Range(0, 13);

        Esfera[] esferas = FindObjectsOfType<Esfera>();


        switch (efecto)
        {
            case 0:
                foreach (Esfera e in esferas)
                {
                    if (Random.Range(0, 100) < 20)
                    {
                        if(e.material!=2)
                            e.DestruirRetardo();
                    }
                }
                break;

            case 1:
            case 2:
                foreach (Esfera e in esferas)
                {
                    if (Random.Range(0, 100) < 20)
                    {
                        
                        e.transform.DOScale(2, 0.1F).SetEase(Ease.OutBounce);
                        if (!e.esfera && e.material == 2)
                        {
                            e.esfera = true;
                            restantes++;
                        }
                    }
                }
                
                break;

            case 3:
                foreach (Esfera e in esferas)
                {
                    if (Random.Range(0, 100) < 20)
                    {
                        e.transform.DOScale(0.5F, 0.1F).SetEase(Ease.OutBounce);
                        if (!e.esfera && e.material == 2)
                        {
                            e.esfera = true;
                            restantes++;
                        }
                    }
                }

                break;

            case 4:
                foreach (Esfera e in esferas)
                {
                    e.transform.DOScale(1F, 0.1F).SetEase(Ease.OutBounce);
                    if (!e.esfera && e.material == 2)
                    {
                        e.esfera = true;
                        restantes++;
                    }
                }

                break;

            case 5:
                transform.GetChild(Random.Range(0, 3)).GetComponent<Renderer>().material = materiales[Random.Range(0, 4)];

                break;

            case 6:
                foreach (Esfera e in esferas)
                {
                    if (Random.Range(0, 100) < 20)
                    {
                        e.GetComponent<Rigidbody>().useGravity = false;
                    }
                }

                break;

            case 7:
                foreach (Esfera e in esferas)
                {
                    if (Random.Range(0, 100) < 20)
                    {
                        e.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(0, 109F), Random.Range(0, 190F), Random.Range(0, 100F)));
                    }
                }

                break;

            case 8:
                foreach (Esfera e in esferas)
                {
                    if (Random.Range(0, 100) < 20)
                    {
                        e.transform.DOScaleX(2, 0.1F).SetEase(Ease.OutBounce);
                        if (e.esfera && e.material==2)
                        {
                            e.esfera = false;
                            restantes--;
                        }
                        
                    }
                }

                break;
            case 9:
                foreach (Esfera e in esferas)
                {
                    if (Random.Range(0, 100) < 20)
                    {
                        Transform t = e.transform;
                        Destroy(e.gameObject);
                        Instantiate(personajeRaro, t.position, t.rotation);

                    }
                }

                break;

            case 10:
                foreach (Esfera e in esferas)
                {
                    if (Random.Range(0, 100) < 20)
                    {

                        e.transform.DOScale(8, 0.1F).SetEase(Ease.OutBounce);
                        if (!e.esfera && e.material == 2)
                        {
                            e.esfera = true;
                            restantes++;
                        }
                    }
                }

                break;
            case 12:
                for (int i = 0; i < transform.childCount; i++)
                {
                    transform.GetChild(i).gameObject.AddComponent<Rigidbody>();
                }

                break;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void Explotada(Esfera e)
    {
        if (e.esfera && e.material == 2)
        {
            aciertos++;
            restantes--;
        }
        else
        {
            fallos++;
        }

        texto1.text = aciertos.ToString();
        texto2.text = fallos.ToString();

        if (restantes <= 0 && !gameOver)
        {
            StartCoroutine(GameOver());


        }

        pop.Play();
    }

    IEnumerator GameOver()
    {

        jugando = false;
        gameOver = true;

        tada.Play();
        yield return new WaitForSeconds(2);

        personaje.GetComponent<Animator>().SetTrigger("entrar");
        slipUp.Play();

        yield return new WaitForSeconds(2);

        bocadillo.transform.GetChild(3).gameObject.SetActive(false);
        bocadillo.transform.GetChild(4).gameObject.SetActive(true);
        bocadillo.SetActive(true);
        audioPlink.Play();
        bocadillo.transform.DOPunchScale(Vector3.one, 0.25F);
        bocadillo.transform.GetChild(5).GetComponent<Button>().interactable = true;
        linea = 2;
    }

    public void Next()
    {
        if (gameOver == false)
        {
            if (linea < 3)
            {
                bocadillo.transform.GetChild(linea).gameObject.SetActive(false);
                bocadillo.transform.DOPunchScale(Vector3.one / 2f, 0.25F);
                linea++;
                if (linea < 4)
                {
                    bocadillo.transform.GetChild(linea).gameObject.SetActive(true);
                }
                if (linea == 3)
                {
                    bocadillo.transform.GetChild(5).GetComponent<Button>().interactable = false;
                    StartCoroutine(Iniciar());
                }
            }
        }


        if(gameOver == true)
        {
            if (linea == 2)
            {
                bocadillo.transform.GetChild(5).GetComponent<Button>().interactable = true;
                bocadillo.transform.GetChild(4).gameObject.SetActive(false);
                bocadillo.transform.GetChild(2).gameObject.SetActive(true);
                linea++;
            }
            else
            {
                bocadillo.transform.GetChild(5).GetComponent<Button>().interactable = false;
                bocadillo.transform.GetChild(2).gameObject.SetActive(false);
                bocadillo.transform.GetChild(3).gameObject.SetActive(true);
                Reiniciar();
            }

        }
    }

    void Reiniciar()
    {
        int aciertos = 0;
        int fallos = 0;
        int restantes = 0;

        int linea;

        bool jugando = false;
        bool gameOver = false;

        texto1.text = "0";
        texto2.text = "0";

        Esfera[] esferas = FindObjectsOfType<Esfera>();
        foreach (Esfera e in esferas)
        {
            Destroy(e.gameObject);
        }

        StopAllCoroutines();
        StartCoroutine(Iniciar());
    }

    public void SumarRestantes(int cantidad)
    {
        restantes += cantidad;
    }
}
