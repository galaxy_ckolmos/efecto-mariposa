using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class Esfera : MonoBehaviour
{
    public GameObject particulas;

    public bool esfera = false;
    public int material = 0;


    private void Start()
    {
        transform.localScale = Vector3.zero;
        transform.DOScale(1, 0.1F).SetEase(Ease.OutBounce);
    }

    private void OnMouseDown()
    {
        if (FindObjectOfType<Generador>().jugando)
        {
            if (Random.Range(0, 100) < 10)
            {
                GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(0, 1000F), Random.Range(0, 100F), Random.Range(0, 1000F)));
            }
            else
            {
                Destruir();
            }
        }
    }

    public void Destruir()
    {
        GameObject p = Instantiate(particulas, transform.position, Quaternion.identity);
        p.GetComponent<ParticleSystem>().startColor = GetComponent<Renderer>().material.color;

        if (Random.Range(0, 100) < 10)
        {
            Instantiate(gameObject,transform.position,Quaternion.identity);
            Instantiate(gameObject, transform.position, Quaternion.identity);
        
            if(esfera && material == 2)
            {
                FindObjectOfType<Generador>().SumarRestantes(2);
            }
        }

        FindObjectOfType<Generador>().Explotada(this);
        gameObject.SetActive(false);
    }

    public void DestruirRetardo()
    {
        Invoke("Destruir", Random.Range(0, 4F));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Inferior")
        {
            Destroy(gameObject);
        }

        AudioSource a = GetComponent<AudioSource>();
        if (a != null)
        {
            GetComponent<AudioSource>().Play();
        }
        
    }
}
